## Description

<!-- Example:
**This MR provides:**
-   X Model was added
-   Unit tests for X have been implemented
-   Migrations for X have been generated
-->

Closes #(issue)

## How to manually test

<!-- Example:
1) First do this
2) Then do this
3) Finally do this

Expected result: The application should do X or be in Y state 
-->