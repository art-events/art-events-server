# Django Backend for the Art Events project


## Environment Requirements

+   python 3.6.9
+   mysql-server
+   mysql-client
+   [virtualenv](https://virtualenv.pypa.io/en/latest/)
+   [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/)
+   setup a virtualenv:

```bash
mkvirtualenvwrapper art-events-backend
```

## Development Environment

### Installation

***Set the virtual environment***

```bash
workon art-events-backend
```

***Install server requirements***

```bash
pip install -r requirements/local.txt
```

### Configuration

#### First steps

**Before applying migrations, creating superusers or running the server, a PostgreSQL database must be created.**

First, you need to expose the following environment variables to configure the django database settings.

Export values to the current terminal.
> Values have to be exported every time you open a new terminal

```bash
export ART_EVENTS_DB_NAME="art_events_django_backend"
export ART_EVENTS_DB_USER="myPostgreSqlUsername"
export ART_EVENTS_DB_PASS="myPostgreSqlPassword"
export ART_EVENTS_DB_HOST="0.0.0.0"
```

You can also place those values in the `~/.bashrc`.

> Once they're in the .bashrc there's no need to export them every time you open a terminal

```bash
echo "
# === My App Name Env ===
export ART_EVENTS_DB_NAME="art_events_django_backend"
export ART_EVENTS_DB_USER="myPostgreSqlUsername"
export ART_EVENTS_DB_PASS="myPostgreSqlPassword"
export ART_EVENTS_DB_HOST="0.0.0.0"
" >> ~/.bashrc
```

> If you continue following instructions in the same terminal you might need
> to execute `exec bash` to reload the environment variables we just declared.

#### Create a Database and Database User

> Taken from digital ocean [community tutorials](https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04#create-a-database-and-database-user)

***We must use the same values we declared in the environment variables for the database name, the postgresql user and the postgresql password.***

By default, Postgres uses an authentication scheme called “peer authentication” for local connections. Basically, this means that if the user’s operating system username matches a valid Postgres username, that user can login with no further authentication.

During the Postgres installation, an operating system user named postgres was created to correspond to the postgres PostgreSQL administrative user. We need to change to this user to perform administrative tasks:

```bash
sudo su - postgres
```

You should now be in a shell session for the postgres user. Log into a Postgres session by typing:

```bash
psql
```

First, we will create a database for our Django project. Each project should have its own isolated database for security reasons. We will call our database myproject in this guide, but it’s always better to select something more descriptive:

```sql
CREATE DATABASE art_events_django_backend;
```

Remember to end all commands at an SQL prompt with a semicolon.

Next, we will create a database user which we will use to connect to and interact with the database. Set the password to something strong and secure:

```sql
CREATE USER myPostgreSqlUsername WITH PASSWORD 'myPostgreSqlPassword';
```

Afterwards, we’ll modify a few of the connection parameters for the user we just created. This will speed up database operations so that the correct values do not have to be queried and set each time a connection is established.

We are setting the default encoding to UTF-8, which Django expects. We are also setting the default transaction isolation scheme to “read committed”, which blocks reads from uncommitted transactions. Lastly, we are setting the timezone. By default, our Django projects will be set to use UTC:

```sql
ALTER ROLE myPostgreSqlUsername SET client_encoding TO 'utf8';
ALTER ROLE myPostgreSqlUsername SET default_transaction_isolation TO 'read committed';
ALTER ROLE myPostgreSqlUsername SET timezone TO 'UTC';
```

Now, all we need to do is give our database user access rights to the database we created:

```sql
GRANT ALL PRIVILEGES ON DATABASE art_events_django_backend TO myPostgreSqlUsername;
```

Exit the SQL prompt to get back to the postgres user’s shell session:

```sql
\q
```

Exit out of the postgres user’s shell session to get back to your regular user’s shell session:

```bash
exit
```


#### Make migrations

Now we're ready to create the needed migrations for this project

> Common errors: the database password is empty in your local.py django settings (it shouldn't!), you accidentally declared a PORT (it should be none) in your local.py django settings or the postgreqsl is not running (run `systemctl start postgresql`).

```bash
python manage.py makemigrations
```

#### Run migrations

We're now ready to migrate the schemas to our database.

```bash
python manage.py migrate
```

#### Make scripts

```bash
# Shows a help dialog
make help
# Wipes the database
make nuke
# Resets your local environment. Useful after switching branches, etc.
make reset
# Executes nuke and reset scripts, then loads fixtures
make nuke-reset
# Like reset but without the wiping installed dependencies.
make clear
# Runs test suites
make test
```

> **Superusers from fixture have the following credentials:**  
> email: your @camba email  
> password: camba  

#### Other commands

***Create super user***

Note that you could have users automatically created if you add them to the accounts fixture.

```bash
# prompts the cli to create a superuser
python manage.py createsuperuser
# compiles i18n translations
python manage.py compilemessages
```

### Running the server

***declare mysql credentials in your terminal or .bashrc***
```bash
export DB_USER=yourMySqlUser
export DB_PASSWORD=yourMySqlPassword
```

***start server normally***
```bash
python manage.py runserver
```


> If you notice some of the admin text fields look weird in the UI that's because you might need to run the compile messages script described in the **Other commands** section in this file.

***using [werkzeug](https://werkzeug.palletsprojects.com/en/0.15.x/tutorial/)***
```bash
python manage.py runserver_plus
```

### Development Workflow

#### Adding new dependencies

New packages can be added using `pip`

```bash
pip install package_name
```

Whenever we add a new package we **must** freeze versions

```bash
pip freeze
```

Search the new added dependency and include it to the proper requirements file.
```
dj-database-url==0.5.0
Django==2.2.5
django-debug-toolbar==2.0
django-extensions==2.2.1
django-mysql==3.2.0
djangorestframework==3.10.3
mysqlclient==1.4.4
pytz==2019.3
six==1.12.0
sqlparse==0.3.0
Werkzeug==0.15.6
```

**Requirements**

+   base: dependencies used through the whole project
+   local: dependencies for local usage (e.g.: debugging tools)
+   production: dependencies used for production (e.g.: production server, static files management, etc.)
+   testing: dependencies for testing only (e.g.: testing framework)
