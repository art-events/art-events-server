from django.contrib import admin
from .models import Tag


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    fieldsets = (
                        (
                             None, {
                                 'classes': ('wide',),
                                 'fields': (
                                     'name',
                                 ),
                             }
                         ),
                     )

    list_display = ('id', 'name', 'slug', 'created_at')
    list_filter = ['slug']
    ordering = ('id', '-created_at')
