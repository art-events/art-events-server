from rest_framework import filters, generics, mixins, viewsets
from .models import Tag
from .serializers import TagSerializer


# Create your views here.
class EventViewSet(
    generics.ListAPIView,
    viewsets.ModelViewSet,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['id', 'created_at']
    ordering = ['id']
