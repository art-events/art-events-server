from django.db import models
from django.template.defaultfilters import slugify
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class DatesMixin(models.Model):
    class Meta:
        abstract = True

    created_at = models.DateTimeField(
        _('created_at'),
        default=timezone.now,
        editable=False,
    )
    updated_at = models.DateTimeField(
        _('updated_at'),
        default=timezone.now,
        editable=False,
    )


class Tag(DatesMixin):
    name = models.CharField(
        _('tags_prop_name'),
        max_length=18, blank=False, default='',
    )
    slug = models.SlugField(unique=True)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        self.slug = slugify(self.name)
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _('tags_model_name')
        verbose_name_plural = _('tags_model_plural_name')
