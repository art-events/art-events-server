from django.conf import settings
from rest_framework import serializers
from .models import Event
from ..tags.serializers import TagSerializer


class EventSerializer(serializers.HyperlinkedModelSerializer):
    picture_file = serializers.SerializerMethodField()
    tags = TagSerializer(many=True)

    def get_picture_file(self, obj):
        picture_file_name = obj.picture_file.name
        is_fixture_image = str.startswith(picture_file_name, 'http')
        if (is_fixture_image):
            return f'{picture_file_name}'
        return settings.MEDIA_URL + picture_file_name

    class Meta:
        model = Event
        fields = (
            'id',
            'title',
            'description',
            'picture_file',
            'start_date',
            'end_date',
            'tags',
            'created_at',
            'updated_at',
        )
