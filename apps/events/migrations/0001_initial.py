# Generated by Django 2.2.5 on 2020-03-29 15:29

import apps.events.models
import django.core.validators
from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('tags', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(default='', max_length=128, verbose_name='events_prop_title')),
                ('picture_file', models.FileField(blank=True, help_text='events_help_text_picture_file', upload_to=apps.events.models.filepath_picture_file, validators=[django.core.validators.FileExtensionValidator(allowed_extensions=['pdf', 'odt', 'doc', 'png', 'jpg']), apps.events.models.sizeFileValidator], verbose_name='events_prop_picture_file')),
                ('description', models.TextField(blank=True, max_length=2500, verbose_name='events_prop_description')),
                ('start_date', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='events_prop_start_date')),
                ('end_date', models.DateTimeField(default=django.utils.timezone.now, editable=False, verbose_name='events_prop_end_date')),
                ('created_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('updated_at', models.DateTimeField(default=django.utils.timezone.now, editable=False)),
                ('tags', models.ManyToManyField(blank=True, default=None, related_name='tags', to='tags.Tag', verbose_name='events_prop_tags')),
            ],
            options={
                'verbose_name': 'events_model_name',
                'verbose_name_plural': 'events_model_plural_name',
                'ordering': ['title'],
            },
        ),
    ]
