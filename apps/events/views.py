from rest_framework import filters, mixins, viewsets
from .models import Event
from apps.tags.models import Tag
from .serializers import EventSerializer


def filter_by_tag(events, tags_array):
    i = 0
    filtered_events = events
    tags_length = len(tags_array)
    while (i < tags_length):
        filtered_events = filtered_events.filter(tags__id__in=[tags_array[i]])
        i = i+1
    return filtered_events


class EventViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['id', 'created_at']
    ordering = ['id']

    def get_queryset(self):
        tags_param = self.request.query_params.get('tags')
        if tags_param is not None:
            tag_list = []
            for tag_param in tags_param.split(','):
                tag_list.append(int(tag_param))
            tags = Tag.objects.filter(id__in=tag_list).values_list(
                'id', flat=True
            )
            events = filter_by_tag(self.queryset, list(tags))
            return events
        return self.queryset
