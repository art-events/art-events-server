from django.contrib import admin
from .models import Event


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'created_at')
    list_filter = ['created_at']
    ordering = ('id', '-created_at')
