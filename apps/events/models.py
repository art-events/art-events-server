from django.conf import settings
from django.db import models
from django.core.exceptions import ValidationError
from django.core.validators import FileExtensionValidator
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

MAX_FILE_SIZE = 1048576
ALLOWED_EXTENSIONS = ['pdf', 'odt', 'doc', 'png', 'jpg']


def sizeFileValidator(file):
    if file.size < MAX_FILE_SIZE:
        return True
    else:
        raise ValidationError('project_file_field_size_help_text')


def filepath_picture_file(instance, filename):
    return filepath(
        instance,
        filename,
        'events/event_{0}/picture/{1}_{2}'
    )


def filepath(instance, filename, path):
    return path.format(
        instance.id,
        timezone.now().strftime('%d_%h-%T'),
        filename,
    )


class Event(models.Model):
    title = models.CharField(
        _('events_prop_title'),
        max_length=128, blank=False, default='',
    )
    picture_file = models.FileField(
        verbose_name=_('events_prop_picture_file'),
        blank=True, help_text=_('events_help_text_picture_file'),
        upload_to=filepath_picture_file,
        validators=[
            FileExtensionValidator(allowed_extensions=ALLOWED_EXTENSIONS),
            sizeFileValidator
        ]
    )
    description = models.TextField(
        verbose_name=_('events_prop_description'),
        max_length=2500, blank=True,
    )
    start_date = models.DateTimeField(
        verbose_name=_('events_prop_start_date'),
        default=timezone.now, editable=False
    )
    end_date = models.DateTimeField(
        verbose_name=_('events_prop_end_date'),
        default=timezone.now, editable=False
    )
    tags = models.ManyToManyField(
        settings.TAG_MODEL,
        related_name='tags',
        verbose_name=_('events_prop_tags'), blank=True, default=None
    )
    created_at = models.DateTimeField(default=timezone.now, editable=False)
    updated_at = models.DateTimeField(default=timezone.now, editable=False)

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        return super(Event, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.title}'

    class Meta:
        ordering = ['title']
        verbose_name = _('events_model_name')
        verbose_name_plural = _('events_model_plural_name')
