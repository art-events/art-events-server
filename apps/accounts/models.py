from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import UserManager
from django.utils import timezone
from django.contrib.auth.models import PermissionsMixin
from django.utils.translation import ugettext_lazy as _


class User(AbstractBaseUser, PermissionsMixin):

    username = models.CharField(
        _('users_prop_username'),
        max_length=30, unique=True
    )
    email = models.EmailField(
        _('users_prop_email_address'),
        max_length=80, unique=True
    )
    first_name = models.CharField(
        _('users_prop_first_name'),
        max_length=30, default=''
    )
    last_name = models.CharField(
        _('users_prop_surname'),
        max_length=30, default=''
    )
    is_active = models.BooleanField(
        _('users_prop_is_active'),
        default=True,
        help_text=(
            'Designates whether this user should be treated as '
            'active. Unselect this instead of deleting users.'
        )
    )
    is_staff = models.BooleanField(
        _('users_prop_is_staff_status'),
        default=True,
        help_text='Designates whether the user can log into this admin site.'
    )
    is_superuser = models.BooleanField(
        _('users_prop_is_super_user'),
        default=False,
    )
    created_at = models.DateTimeField(
        _('created_at'),
        default=timezone.now,
        editable=False
    )
    updated_at = models.DateTimeField(
        _('updated_at'),
        default=timezone.now,
        editable=False
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ('username', 'last_name')

    objects = UserManager()

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        super(User, self).save(*args, **kwargs)

    def __str__(self):
        return self.get_username()

    class Meta:
        db_table = 'auth_user'
        verbose_name = _('users_model_name')
        verbose_name_plural = _('users_model_plural_name')


class Account(models.Model):

    device_id = models.CharField(
        _('accounts_prop_device_id'),
        max_length=60,
        unique=True,
        editable=False
    )
    push_token = models.CharField(
        _('accounts_prop_push_token'),
        max_length=60,
        unique=True,
    )
    created_at = models.DateTimeField(
        _('created_at'),
        default=timezone.now,
        editable=False
    )
    updated_at = models.DateTimeField(
        _('updated_at'),
        default=timezone.now,
        editable=False
    )

    def save(self, *args, **kwargs):
        ''' On save, update timestamps '''
        if not self.id:
            self.created_at = timezone.now()
        self.updated_at = timezone.now()
        super(Account, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.id}'

    class Meta:
        verbose_name = _('accounts_model_name')
        verbose_name_plural = _('accounts_model_plural_name')
