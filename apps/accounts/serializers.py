from django.contrib.auth.models import Group
from django.contrib.auth.hashers import make_password
from rest_framework import serializers
from .models import Account, User


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('name',)


class UserSerializer(serializers.HyperlinkedModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    id = serializers.ReadOnlyField()
    password = serializers.CharField(
        min_length=6,
        write_only=True,
        required=False,
        style={'input_type': 'password'},
    )
    old_password = serializers.CharField(
        min_length=4,
        required=False,
        style={'input_type': 'password'},
    )
    username = serializers.CharField(
        required=False
    )
    email = serializers.CharField(
        required=False
    )

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'groups',
            'first_name',
            'last_name',
            'created_at',
            'updated_at',
            'password',
            'old_password',
        )

    def create(self, validated_data):
        validated_data['password'] = make_password(
            validated_data.get('password')
        )
        return super(UserSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.username = validated_data.get('content', instance.content)
        instance.save()
        return instance


class AccountSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Account
        fields = (
            'id',
            'device_id',
        )
