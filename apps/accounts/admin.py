from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import Account, User


@admin.register(User)
class UserAdmin(UserAdmin):
    list_display = (
        'id', 'username', 'email', 'first_name', 'last_name', 'is_staff'
    )
    list_filter = ('created_at',)
    add_fieldsets = (
                        (
                             None, {
                                 'classes': ('wide',),
                                 'fields': (
                                     'username', 'password1', 'password2',
                                 ),
                             }
                         ),
                        (
                             _('users_admin_personal_info'), {
                                 'classes': ('wide',),
                                 'fields': (
                                     'email', 'first_name', 'last_name',
                                 ),
                             }
                         ),
                     )
    fieldsets = (
                    (
                         None, {
                             'classes': ('wide',),
                             'fields': (
                                 'username', 'password',
                             ),
                         }
                     ),
                     (
                        _('users_admin_personal_info'), {
                           'classes': ('wide',),
                           'fields': (
                               'first_name', 'last_name', 'email',
                           )
                        }
                     ),
                     (
                        _('users_admin_user_permissions'), {
                           'classes': ('wide',),
                           'fields': (
                               'is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions',
                           )
                        }
                     ),
                     (
                        _('relevant_dates'), {
                           'classes': ('',),
                           'fields': (
                               'last_login',
                           )
                        }
                     ),
                 )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_filter = ('device_id',)
