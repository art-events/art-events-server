from django.contrib.auth.models import Group
from rest_framework import filters, generics, mixins, viewsets
from rest_framework.decorators import (
    action, api_view, authentication_classes, permission_classes,
)
from rest_framework.response import Response
from .models import Account, User
from .serializers import AccountSerializer, GroupSerializer, UserSerializer


# Create your views here.
class AccountViewSet(
    generics.ListAPIView,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet
):

    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['id', 'created_at']
    ordering = ['id']

    @action(methods=['get'], detail=False, name='device')
    def device(self, request):
        device_id = self.request.headers['Device-Id']
        push_token = self.request.headers['Expo-Push-Token']
        if device_id is not None:
            account, is_new = Account.objects.get_or_create(
                device_id=device_id,
                push_token=push_token
            )
            serialized_account = self.get_serializer(account)
            status = 201 if is_new else 200
            data = serialized_account.data
            data['status'] = status
            return Response(status=status, data=data)
        return Response(status=404, data={'message-slug': 'not-found'})


class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer


class UserViewSet(viewsets.ModelViewSet, generics.UpdateAPIView):
    queryset = User.objects.all().order_by('-created_at')
    serializer_class = UserSerializer
    filter_fields = ('username',)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get('pk')
        if pk == 'current':
            self.kwargs['pk'] = str(request.user.pk)
        return super(UserViewSet, self).retrieve(request, pk=pk)

    def update(self, request, *args, **kwargs):
        user = User.objects.get(email=request.user.email)
        self.object = self.get_object()
        if user.id != self.object.id:
            response = {
                'code': 403,
                'data': [],
                'message': 'Not authorized to perform this action',
                'status': 'failure',
            }
            return Response(response)

        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            # Check old password
            old_password = serializer.data.get("old_password")
            if not self.object.check_password(old_password):
                response = {
                    'code': 400,
                    'old_password': ['Wrong password.'],
                    'status': 'failure',
                }
                return Response(response)

            # set_password also hashes the password that the user will get
            if 'password' in request.data:
                self.object.set_password(request.data['password'])
            self.object.save()
            response = {
                'code': 200,
                'data': [],
                'message': 'Succesfully update',
                'status': 'success',
            }
            return Response(response)

        response = {
            'code': 400,
            'data': serializer.errors,
            'status': 'failure',
        }
        return Response(response)


@api_view(['POST'])
@authentication_classes([])
@permission_classes([])
def signup(request):
    serialized = UserSerializer(data=request.data)
    if serialized.is_valid():
        serialized.save()
        return Response(serialized.data, status=201)
    else:
        return Response(serialized._errors, status=400)
