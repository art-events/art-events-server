# Generated by Django 2.2.5 on 2020-04-01 00:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_auto_20200331_2157'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='push_token',
            field=models.CharField(editable=False, max_length=60, unique=True, verbose_name='accounts_prop_push_token'),
            preserve_default=False,
        ),
    ]
