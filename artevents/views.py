from collections import OrderedDict
from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    page_size = 18
    page_size_query_param = 'per_page'
    max_page_size = 50

    def get_paginated_response(self, data):
        per_page = self.page.paginator.per_page
        page_count = self.page.paginator.count
        if page_count % per_page:
            last_page = page_count/per_page + 1
        else:
            last_page = page_count/per_page
        from_item = (self.page.number-1) * per_page
        if int(last_page) == self.page.number and page_count % per_page:
            to_item = from_item + (page_count % per_page)
        else:
            to_item = self.page.number * per_page
        return Response(OrderedDict([
            ('per_page', per_page),
            ('current_page', self.page.number),
            ('last_page', int(last_page)),
            ('from', from_item),
            ('to', to_item),
            ('next_page_url', self.get_next_link()),
            ('prev_page_url', self.get_previous_link()),
            ('total', page_count),
            ('results', data)
        ]))
