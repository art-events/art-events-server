"""artevents URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.apps import apps
from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.views.generic import RedirectView
from django.conf.urls.i18n import i18n_patterns
from rest_framework import routers
from rest_framework_jwt.views import (
    obtain_jwt_token, refresh_jwt_token, verify_jwt_token
)
from apps.accounts import views as accountViews
from apps.events import views as eventViews
from apps.tags import views as tagViews

# Routers provide an easy way of automatically determining the URL conf.
ROUTER = routers.DefaultRouter()
ROUTER.register(r'accounts', accountViews.AccountViewSet)
ROUTER.register(r'events', eventViews.EventViewSet)
ROUTER.register(r'groups', accountViews.GroupViewSet)
ROUTER.register(r'tags', tagViews.EventViewSet)
ROUTER.register(r'users', accountViews.UserViewSet)


urlpatterns = i18n_patterns(
    url(r'^$', RedirectView.as_view(url='/admin/')),
    path('admin/', admin.site.urls)
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Router Patterns
urlpatterns = urlpatterns + [
    url(r'^api/', include(ROUTER.urls)),
]

# Auth patterns
urlpatterns = urlpatterns + [
    url(
        r'^api-auth/',
        include('rest_framework.urls', namespace='rest_framework')
    ),
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-verify/', verify_jwt_token),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^api-signup/', accountViews.signup)
]

if settings.DEBUG:
    from django.views.defaults import page_not_found
    urlpatterns += i18n_patterns(
        url(r'^404/$', page_not_found),
    )

# Only enable debug toolbar if it's an installed app
if apps.is_installed('debug_toolbar'):
    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
