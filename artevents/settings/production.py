import os
from .base import *  # noqa

ALLOWED_HOSTS = ['*']

ALLOWED_HOSTS = os.environ.get('ALLOWED_HOSTS', '').split(' ')

DEBUG = False

# Logging

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        'django': {
            'handlers': ['console'],
            'level': os.getenv('DJANGO_LOG_LEVEL', 'DEBUG'),
        },
    },
}

SECRET_KEY = os.environ.get('ART_EVENTS_SECRET_KEY')

# Use cached templates in production
TEMPLATES[0]['APP_DIRS'] = False
TEMPLATES[0]['OPTIONS']['loaders'] = [
    (
        'django.template.loaders.cached.Loader', [
            'django.template.loaders.filesystem.Loader',
            'django.template.loaders.app_directories.Loader',
        ]
    ),
]

# SSL required for session/CSRF cookies
CSRF_COOKIE_SECURE = True
SESSION_COOKIE_SECURE = True

URL_SCHEME = 'https'

MIDDLEWARE += [
    'whitenoise.middleware.WhiteNoiseMiddleware',
]

if os.environ.get('API_URL'):
    API_URL = os.environ.get('API_URL')

if os.environ.get('HEROKU_ENV'):

    DATABASE_URL = os.environ.get('DATABASE_URL')
    DATABASES = {
        'default': dj_database_url.config(default=DATABASE_URL),
    }

if os.environ.get('DOCKER_COMPOSE'):
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': os.environ.get('ART_EVENTS_MYSQL_DB_NAME'),
            'USER': os.environ.get('ART_EVENTS_MYSQL_USER'),
            'PASSWORD': os.environ.get('ART_EVENTS_MYSQL_PASS'),
            'HOST': os.environ.get('ART_EVENTS_MYSQL_HOST'),
            'PORT': os.environ.get('ART_EVENTS_MYSQL_PORT'),
            'OPTIONS': {
                'init_command': (
                    'SET storage_engine=InnoDB;'
                    f'ALTER DATABASE {os.environ.get("ART_EVENTS_MYSQL_DB_NAME")} CHARACTER SET utf8;'
                ),
            }
        }
    }
