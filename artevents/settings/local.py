import os
from importlib import import_module
from .base import *

DEBUG = True
TEMPLATES[0]['OPTIONS']['debug'] = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ.get('ART_EVENTS_MYSQL_DB_NAME'),
        'USER': os.environ.get('ART_EVENTS_MYSQL_USER'),
        'PASSWORD': os.environ.get('ART_EVENTS_MYSQL_PASS'),
        'HOST': os.environ.get('ART_EVENTS_MYSQL_HOST'),
        'PORT': '',
    }
}

INTERNAL_IPS = ['127.0.0.1']
ALLOWED_HOSTS = ['*']

INSTALLED_APPS += [
    'django_extensions',
]

# Use vanilla StaticFilesStorage to allow tests to run outside of tox easily
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

SECRET_KEY = 'artevents'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

# Django debug toolbar - show locally unless DISABLE_TOOLBAR is enabled with environment vars
# eg. DISABLE_TOOLBAR=1 ./manage.py runserver
if not os.environ.get('DISABLE_TOOLBAR'):
    INSTALLED_APPS += [
        'debug_toolbar',
    ]

    MIDDLEWARE = [
        'debug_toolbar.middleware.DebugToolbarMiddleware',
    ] + MIDDLEWARE

if os.environ.get('API_URL'):
    API_URL = os.environ.get('API_URL')
